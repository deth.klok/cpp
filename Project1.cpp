
#include <iostream>
class Animal
{
public:
    
    virtual void voise() const = 0;
};

class Cat : public Animal
{
public:
    void voise() const override
    {
        std::cout << "Meow\n";
    }
};

class Dog : public Animal
{
public:
    void voise() const override
    {
        std::cout << "Woof\n";
    }
};

class Goose : public Animal
{
public:
    void voise() const override
    {
        std::cout << "GA\n";
    }
};

int main()
{
    Animal* animals[3];
    animals[0] = new Cat();
    animals[1] = new Dog();
    animals[2] = new Goose();

    for (Animal* a : animals)
        a->voise();
}